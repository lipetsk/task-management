package main

import (
	"database/sql"  // for database use
	"encoding/json" // for JSON HTTP-transactions
	"net/http"      // for HTTP-server
	"os"            // for read system environments
	"strings"       // parsing HTTP-headers
	"text/template" // for use HTML-page templates
	"time"          // for timestamps

	"github.com/lib/pq"              // for using PostgreSQL
	uuid "github.com/satori/go.uuid" // for UUID operations id database
)

// ===========================================================================================================================
// Common system routines
// ===========================================================================================================================

// Return timestamp of current server-side application executable file in 'YYYY.MM.DD.hh.mm.ss' format
func GetBuildVersion() string {
	fileName, _ := os.Executable()
	fileInfo, _ := os.Stat(fileName)
	fileTime := fileInfo.ModTime()
	return fileTime.Format("2006-02-01 15:04:05")
}

// ===========================================================================================================================
// Common database routines
// ===========================================================================================================================

var DB *sql.DB
var DATABASECONNECTION string

// Check database connection and reconnect if necessary.
// Store database link into global variable DB
func CheckDatabaseConnection() {
	var err error
	if DB != nil {
		err := DB.Ping()
		if err == nil {
			return
		}
		DB.Close()
	}
	DB, err = sql.Open("postgres", DATABASECONNECTION)
	if err != nil {
		panic(err)
	}
	err = DB.Ping()
	if err != nil {
		panic(err)
	}
}

// ===========================================================================================================================
// Common Web-server routines
// ===========================================================================================================================

// Add no-cache header to http read-write streams
func DontCache(res http.ResponseWriter) {
	res.Header().Add("Cache-Control", "no-cache")
}

// Add no-cache header to file-server handler
func DontCacheHandler(h http.Handler) http.HandlerFunc {
	return func(res http.ResponseWriter, req *http.Request) {
		// Set header
		DontCache(res)
		// Serve with the actual handler
		h.ServeHTTP(res, req)
	}
}

// Parse JSON from http-request to a struct
// IN:	req - http.Request, from which JSON is parsed
// 		structJSON - structure  to which JSON is parsed
// OUT: -
func ParseJSON(req *http.Request, structJSON interface{}) {
	err := json.NewDecoder(req.Body).Decode(structJSON)
	if err != nil {
		panic(err)
	}
}

// Return JSON in response to a http-request
// IN:	res - http.Responsewriter, in which the returned json is written
// 		structJSON - structure to convert to JSON
// OUT: -
func ReturnJSON(res http.ResponseWriter, structJSON interface{}) {
	resJSON, err := json.Marshal(structJSON)
	if err != nil {
		panic(err)
	}
	DontCache(res)
	res.Header().Set("Content-type", "application/json; charset=utf-8")
	res.Write(resJSON)
}

// If found incoming HTTP-request through cloud HTTPS-proxy (by X-Forwarded-Proto header)
// then redirect user's browser from http to https
// IN: http.ResponseWriter, *http.Request
// OUT: bool [true - redirect required, false - you can continue to serve the request]
func RedirectIncomingHTTPandWWW(res http.ResponseWriter, req *http.Request) bool {
	hostOriginal := strings.ToLower(req.Host)
	hostWithoutWWW := hostOriginal
	if hostOriginal[0:4] == "www." {
		hostWithoutWWW = hostOriginal[4:]
	}
	if (strings.ToLower(req.Header.Get("X-Forwarded-Proto")) == "http") || (hostOriginal != hostWithoutWWW) {
		target := "https://" + hostWithoutWWW + req.URL.Path
		if len(req.URL.RawQuery) > 0 {
			target += "?" + req.URL.RawQuery
		}
		http.Redirect(res, req, target, http.StatusMovedPermanently)
		return true
	} else {
		return false
	}
}

// ===========================================================================================================================
// WEB-PAGE: main page of web-application
// GET /
// ===========================================================================================================================

// Structure to fill HTML-template of main web-page
type typeWebFormData struct {
	BuildVersion string // datetime of current application compiled binaries. Used to prevent some browsers from caching .js & .css files
}

func webMainPage(res http.ResponseWriter, req *http.Request) {

	// If necessary, we redirect requests received via the unprotected HTTP protocol to HTTPS and redirect to hostname without "www.""
	if RedirectIncomingHTTPandWWW(res, req) {
		return
	}

	// All calls to unknown url paths should return 404
	if req.URL.Path != "/" {
		http.NotFound(res, req)
		return
	}

	// Add no-cache header to file-server handler
	DontCache(res)

	// Prepare main structure of HTML-template
	var webFormData typeWebFormData
	webFormData.BuildVersion = GetBuildVersion()

	// Apply HTML-template
	res.Header().Set("Content-type", "text/html")
	t := template.New("main.html").Delims("[[", "]]")
	t, err := t.ParseFiles("./templates/main.html")
	if err != nil {
		panic(err)
	}
	err = t.Execute(res, webFormData)
	if err != nil {
		panic(err)
	}
}

// ===========================================================================================================================
// FILE: service-worker javascript file
// GET /serviceworker.js
// ===========================================================================================================================

func webServiceWorker(res http.ResponseWriter, req *http.Request) {
	DontCache(res)
	http.ServeFile(res, req, "./static/serviceworker.js")
}

// ===========================================================================================================================
// API: Synchronizes tasks between client application and database
// POST /SyncTasks
// Checks the current session for validity. If the session is not valid, it returns `"SessionEmptyNotFoundOrExpired"` as a result.
// Checks all tasks, received from client: adds new (ID ), updates existing.
// Cookies: User-Session : string (UUID)
// IN: JSON: { FromTimestamp datetime,
//             UpdatedOnClientTasks []{ ID: UUID,
//                                      Email: string,
//                                      Content: string,
//                                      Private: boolean,
//                                      Status: string ["draft", "created", "done-but-not-verified", "need-help", "done-and-verified", "canceled", "moved"],
//                                      Revision: integer,
//                                      Timestamp: datetime, // previous task timestamp before editing
//                                      ExternalID: string,
//                                      Assigned: string,
//                                      Term: string, // YYYY-MM-DD
//                                      Icon: string ["wait","remind","call","force","mail","prepare","manage","meet","visit","make","journey","think"],
//                                      Project: string,
//                                      Protocol: string,
//                                      ParentID: UUID or null
//                                    },
//				UpdatedOnClientTodays []{ Email: string;
//										  ID: UUID,
//										  Delimiter: boolean,
//								  		  Index: float, // or -1 to remove task
//										  Todaystamp: datetime //previous today's record timestamp before editing
//										}
// OUT: JSON: { Result: string ["SessionEmptyNotFoundOrExpired", "SynchronizationDone"],
//              UpdatedOnServerTasks []{ ID: UUID,
//                                      Email: string,
//                                      Content: string,
//                                      Private: boolean,
//                                      Status: string ["draft", "created", "done-but-not-verified", "need-help", "done-and-verified", "canceled", "moved"],
//                                      Revision: integer,
//                                      Timestamp: datetime, // new task stamp after writing to database
//                                      ExternalID: string,
//                                      Assigned: string,
//                                      Term: string, // YYYY-MM-DD
//                                      Icon: string ["wait","remind","call","force","mail","prepare","manage","meet","visit","make","journey","think"],
//                                      Project: string,
//                                      Protocol: string,
//                                      ParentID: string // UUID or ""
//                                     },
//				FullActualTodays []{ Email: string;
//									 ID: UUID,
//									 Delimiter: boolean,
//									 Index: float,
//									 Todaystamp: datetime
//								   }
// ===========================================================================================================================

// Structure of task
type typeTask struct {
	ID         string
	Email      string
	Content    string
	Private    bool
	Status     string
	Revision   int
	Timestamp  time.Time
	ExternalID string
	Assigned   string
	Term       string
	Icon       string
	Project    string
	Protocol   string
	ParentID   string
}
type typeToday struct {
	Email      string
	ID         string
	Delimiter  bool
	Index      float64
	Todaystamp time.Time
}

// Structure JSON-request for synchronization
type typeSyncTasksJSONRequest struct {
	FromTimestamp         time.Time
	UpdatedOnClientTasks  []typeTask
	UpdatedOnClientTodays []typeToday
}

// Structure JSON-response for synchronization
type typeSyncTasksJSONResponse struct {
	Result               string
	UpdatedOnServerTasks []typeTask
	FullActualTodays     []typeToday
}

func webSyncTasks(res http.ResponseWriter, req *http.Request) {

	// Parse request to struct and preparing to response
	var request typeSyncTasksJSONRequest
	ParseJSON(req, &request)
	//fmt.Printf("%+v\n", request)
	var response typeSyncTasksJSONResponse

	// Initializing or continuing to use the database connection
	CheckDatabaseConnection()

	// Loop through the rows of tasks submitted by the client side for updating
	for _, tsk := range request.UpdatedOnClientTasks {
		// !!!!!!!!!!!! - add user filtration

		// Convert term from string YYYY-MM-DD to PostgreSQL "Date" type
		var term pq.NullTime
		term.Valid = false
		tim, err := time.Parse("2006-01-02", tsk.Term)
		if (err == nil) && !(tim.IsZero()) {
			term.Scan(tim)
		}
		// Convert UUID to binary format
		var taskID uuid.UUID
		taskID, err = uuid.FromString(tsk.ID)
		if err != nil {
			panic(err)
		}
		// Convert ParentID to nullable binary format
		var parentID uuid.NullUUID
		if tsk.ParentID != "" {
			err = parentID.Scan(tsk.ParentID)
			if err != nil {
				panic(err)
			}
		}

		// Prepare SQL-request
		var sql string
		if tsk.Timestamp.IsZero() {
			sql = `INSERT INTO tasks (id, email, content, private, status, revision, 
									  timestamp, external_id, assigned, term, icon, 
									  project, protocol, parent_id)
	   						  VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)`
		} else {
			sql = `UPDATE tasks SET content=$3, private=$4, status=$5, revision=$6, 
									timestamp=$7, external_id=$8, assigned=$9, term=$10, icon=$11, 
									project=$12, protocol=$13, parent_id=$14
	 							WHERE id=$1 AND email=$2`
		}
		// !!!!!!! - compare timestamps

		// Exec SQL-request
		_, err = DB.Exec(sql, taskID, tsk.Email, tsk.Content, tsk.Private, tsk.Status, tsk.Revision,
			time.Now().UTC(), tsk.ExternalID, tsk.Assigned, term, tsk.Icon,
			tsk.Project, tsk.Protocol, parentID)

		// Parse errors
		if err != nil {
			panic(err)
		}
	}

	// Loop through the rows of tasks submitted by the client side for updating
	for _, td := range request.UpdatedOnClientTodays {
		// !!!!!!!!!!!! - add user filtration
		// Convert UUID to binary format
		var taskID uuid.UUID
		taskID, err := uuid.FromString(td.ID)
		if err != nil {
			panic(err)
		}
		// Prepare SQL-request
		var sql string
		if td.Index < 0 {
			sql = `DELETE FROM todays WHERE email=$1 AND ID=$2`
			_, err = DB.Exec(sql, td.Email, taskID)
		} else {
			sql = `INSERT INTO todays (email, id, delimiter, index, todaystamp)
							   VALUES ($1, $2, $3, $4, $5)
						  ON CONFLICT (email, id) DO UPDATE
						  		  SET index=$4, todaystamp=$5`
			_, err = DB.Exec(sql, td.Email, taskID, td.Delimiter, td.Index, time.Now().UTC())
		}
		// !!!!!!! - compare timestamps
		// Parse errors
		if err != nil {
			panic(err)
		}
	}

	// Prepare and run SQL query // !!!!!!!!!!!! - add user filtration
	sqlSelectTasks := `SELECT id, email, content, private, status, revision, 
							  timestamp, external_id, assigned, term, icon, 
							  project, protocol, parent_id 
					   FROM tasks`
	rows, err := DB.Query(sqlSelectTasks)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	// Enumerating SQL query strings
	for rows.Next() {
		// For each line, we create a task structure that will be converted into a JSON response
		var task typeTask
		var term pq.NullTime
		var parentID sql.NullString
		err := rows.Scan(
			&task.ID, &task.Email, &task.Content, &task.Private, &task.Status, &task.Revision,
			&task.Timestamp, &task.ExternalID, &task.Assigned, &term, &task.Icon,
			&task.Project, &task.Protocol, &parentID)
		if err != nil {
			panic(err)
		}
		if term.Valid {
			task.Term = term.Time.Format("2006-01-02")
		}
		if parentID.Valid {
			task.ParentID = parentID.String
		}
		// Add a record to the resulting array of structures
		response.UpdatedOnServerTasks = append(response.UpdatedOnServerTasks, task)
	}
	err = rows.Err()
	if err != nil {
		panic(err)
	}

	// Prepare and run SQL query // !!!!!!!!!!!! - add user filtration
	sqlSelectTodays := `SELECT email, id, delimiter, index, todaystamp FROM todays ORDER BY index`
	rows, err = DB.Query(sqlSelectTodays)
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	// Enumerating SQL query strings
	for rows.Next() {
		// For each line, we create a task structure that will be converted into a JSON response
		var td typeToday
		err := rows.Scan(
			&td.Email, &td.ID, &td.Delimiter, &td.Index, &td.Todaystamp)
		if err != nil {
			panic(err)
		}
		// Add a record to the resulting array of structures
		response.FullActualTodays = append(response.FullActualTodays, td)
	}
	err = rows.Err()
	if err != nil {
		panic(err)
	}

	// Add to response result status
	response.Result = "SynchronizationDone"

	// Return JSON response
	ReturnJSON(res, response)
}

// ===========================================================================================================================
// Main program: start the web-server
// ===========================================================================================================================
func main() {
	// Read environment variables
	LISTENPORT := os.Getenv("LISTEN_PORT")
	DATABASEHOST := os.Getenv("DATABASE_HOST")
	DATABASEPORT := os.Getenv("DATABASE_PORT")
	DATABASEUSER := os.Getenv("DATABASE_USER")
	DATABASEPASSWORD := os.Getenv("DATABASE_PASSWORD")
	DATABASENAME := os.Getenv("DATABASE_NAME")
	DATABASECONNECTION = "postgres://" + DATABASEUSER + ":" + DATABASEPASSWORD + "@" + DATABASEHOST + ":" + DATABASEPORT + "/" + DATABASENAME + "?sslmode=disable"

	// Assign handlers for web requests
	http.HandleFunc("/SyncTasks", webSyncTasks)
	http.HandleFunc("/serviceworker.js", webServiceWorker)
	http.HandleFunc("/", webMainPage)

	// Register a HTTP file server for delivery static files from the static directory
	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/static/", DontCacheHandler(http.StripPrefix("/static/", fs)))

	CheckDatabaseConnection()
	defer DB.Close()

	// Launch the web server on all interfaces on the PORT port
	http.ListenAndServe(":"+LISTENPORT, nil)
}
