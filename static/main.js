window.onload = function () {
  
  // Try to use Service Worker API.
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/serviceworker.js')
      .then( function() { navigator.serviceWorker.ready.then( function(worker) {
        worker.sync.register('todoworks');
      }) })
      .catch(function(err) { console.log(err) });
  }

  // Create Vue application object
  var app = new Vue({

    // Bind Vue object to global div-container
    el: '#application-main',

    // ========================================================================
    // Data object, containts all data of task manager's window realtime status
    // ========================================================================
    data: function() {
      return {
        user: "", // Email of authorized user
        SID: "", // Session id of authorized user
        languages: [ // Aviable languages
          {languageTag: "en", englishName: "English", nationalName: "English"},
          {languageTag: "ru", englishName: "Russian", nationalName: "Русский"}
        ],
        language: "", // Tag of selected language
        labels: { }, // Text labels of selected languages
        viewMode: "tasks", // Current view mode: ["tasks", "projects", "powerpoint", "protocols", "revisions"]
        viewType: "all-actual", // Current view type: ["public-7days", "only-public", "all-actual", "all"]
        viewFilter: "", // Current applied string filter
        status: "Loading", // Status of synchronization: ["Loading", "TryPartSync", "TryFullSync", "SyncDone", "SyncFailed"]
        awaitingSync: "", // String with count of tasks, awaiting for synchronization
        statistic: { }, // Named rray of integers statistics counter, calulated by method updateStatistic()
        tasks: { }, // All tasks, aviable for current user: named array of structures:
                    // UUID { ID: UUID, 
                    //        Email: string, 
                    //        Content: string, 
                    //        Private: bool,
                    //        Status: string, 
                    //        Revision: integer, 
                    //        Timestamp: string (timestamp original task record, fetched from server; undefined for new tasks),
                    //        ExternalID: string, 
                    //        Assigned: string, 
                    //        Term: string (YYYY-MM-DD),
                    //        Icon: string, 
                    //        Project: string, 
                    //        Protocol: string, 
                    //        ParintID: string (UUID or '') }
        todays: [ ], // All today's tasks: array of tasks structures with additionals attributes:
                     // [ { * all attributes from task structure,
                     //     Delimiter: bool
                     //     Index: float (position in today's list > 0),
                     //     Todaystamp: string (timestamp original today's record, fetched from server; undefined for new today's record) } ]
        lastSynchronizedTimestamp: undefined, // Latest timestamp of tasks or todays, synchronized from server
        currentDate: "", // Current date in 'YYYY-MM-DD' format, initialized during mounted event
        horizonDate: "", // Highest vivsible date in 'public-7days' mode, initialized during mounted event
        task: { }, // Structure of current edited task: ID: UUID, Email: string, ...
        today: false, // bool flag, indicating the presence of current edited task in today's list
        tasksToUpdate: { }, // Named array of tasks to be updated:
                            // UUID : link to same structure in tasks named array
        todaysToUpdate: { }, // Named array of today's tasks records, awayting updates:
                             // UUID { Index: float or -1 if task was removed from today's list
                             //        Delimiter: boolean
                             //        Todaystamp: of today's record fetched from server; undefined for new today's records }
        currentRevision: 0, // Current revision value, used to create new tasks. 
                           // Calculated as maximum revision value of tasks, created by current user (no other people's tasks)
        targetRevision: 0 // Target revision value to audit all your taksk in "revisions" mode
                          // Usually targetRevision = currentRevision + 1
      };
    },

    // ========================================================================
    // Computed fields
    // ========================================================================
    computed: {

      // -------------------------------------------------------------------------
      // Generate list of 4 columns for current tasks
      // [
      //   {class:class1, labelH3:label1, status:status1, items:[task1,task2,..]},
      //   {class:class2, labelH3:label2, status:status2, items:[task1,task2,..]},
      //   ...
      // ]
      // -------------------------------------------------------------------------
      myTasks: function() {
        var columns = [];
        columns.push( { class:"assigned-to-me-tasks", 
                        labelH3:this.labels.labelAssignedToMeTasks, 
                        labelsH4:this.labels.labelsEmptyAssignedToMeTasks,
                        status:"", 
                        items:[] } );
        columns.push( { class:"my-tasks", 
                        labelH3:this.labels.labelMyTasks, 
                        labelsH4:this.labels.labelsEmptyMyTasks,
                        status:"created", 
                        items:[] } );
        columns.push( { class:"assigned-by-me-tasks", 
                        labelH3:this.labels.labelAssignedByMeTasks, 
                        labelsH4:this.labels.labelsEmptyAssignedByMeTasks,
                        status:"created", 
                        items:[] } );
        columns.push( { class:"draft-tasks", 
                        labelH3:this.labels.labelDraftTasks, 
                        labelsH4:this.labels.labelsEmptyDraftTasks,
                        status:"draft", 
                        items:[] } );
        // Enumerating tasks
        for (var id in this.tasks) {
          if ( this.canSeeTask(this.tasks[id]) ) {
            if (this.tasks[id].Status=='draft') { 
              columns[3].items.push(this.tasks[id]);
            } else if (this.tasks[id].Assigned==this.user) {
              columns[0].items.push(this.tasks[id]);
            } else if ( (this.tasks[id].Assigned) && (this.tasks[id].Assigned!=this.user) ) {
              columns[2].items.push(this.tasks[id]);
            } else {
              columns[1].items.push(this.tasks[id]);
            }
          }
        };
        // Sort tasks
        columns[0].items.sort(sortAssignedToMeTasks);
        columns[1].items.sort(sortMyTasks);
        columns[2].items.sort(sortAssignedByMeTasks);
        columns[3].items.sort(sortDraftTasks);
        return columns;
      },

      // -------------------------------------------------------------------------
      // Generate list of N columns of projects for current tasks with its weights
      // [
      //   [ {name:project1, items:[task1,task2,..]}, {name:project2, items:[task1,task2,..]}, ... ],
      //   [ {name:project3, items:[task1,task2,..]}, {name:project4, items:[task1,task2,..]}, ... ],
      //   ...
      // ]
      // -------------------------------------------------------------------------
      projectsTasks4: function() {
        return this.projectsTasks(4);
      },
      projectsTasks5: function() {
        return this.projectsTasks(5);
      },

      // -------------------------------------------------------------------------
      // Generate array of projects names (strings)
      // [ project1, project2, ... ]
      // -------------------------------------------------------------------------
      projects: function() {
        // Compile a list of unique projects
        var foundProjects = {};
        for (var id in this.tasks) {
          if ( (this.tasks[id].Status!='canceled') && (this.tasks[id].Status!='moved') ) {
            if (this.tasks[id].Project) foundProjects[this.tasks[id].Project] = true;
          }
        };
        // Convert to flat list
        var listProjects = [];
        for (var project in foundProjects) {
          listProjects.push(project);
        };
        // Sort list
        listProjects.sort();
        return listProjects;
      },

      // -------------------------------------------------------------------------
      // Generate array of employees names (strings)
      // [ employee1, employee2, ... ]
      // -------------------------------------------------------------------------
      employees: function() {
        // Compile a list of unique employees
        var foundEmployees = {};
        for (var id in this.tasks) {
          if ( (this.tasks[id].Status!='done-and-verified') && (this.tasks[id].Status!='canceled') && (this.tasks[id].Status!='moved') ) {
            if (this.tasks[id].Assigned) foundEmployees[this.tasks[id].Assigned] = true;
          }
        };
        // Convert to flat list
        var listEmployees = [];
        for (var employee in foundEmployees) {
          listEmployees.push(employee);
        };
        // Sort list
        listEmployees.sort();
        return listEmployees;
      },

      // -------------------------------------------------------------------------
      // Generate list of 2 revisions with projects and tasks inside
      // {
      //   // Sources revision
      //   src: {
      //          taskscount: 100,
      //          projects: [
      //                      {name:name1, items:[task1,task2,..]},
      //                      {name:name2, items:[task1,task2,..]},
      //                      {name:name3, items:[task1,task2,..]}
      //                    ]
      //        },
      //   // Destination revision
      //   dst: {
      //          taskscount: 120,
      //          projects: [
      //                      {name:name1, items:[task1,task2,..]},
      //                      {name:name2, items:[task1,task2,..]},
      //                      {name:name3, items:[task1,task2,..]}
      //                    ]
      //        }
      // }
      // -------------------------------------------------------------------------
      revisions: function() {
        // Generate numeric arrays of tasks, that can be seen
        var sortedTasksSrc = [], sortedTasksDst = [], id;
        for (id in this.tasks) {
          if ( this.canSeeTask(this.tasks[id]) ) {
            if (this.tasks[id].Revision<this.targetRevision) {
              sortedTasksSrc.push(this.tasks[id]);
            } else {
              sortedTasksDst.push(this.tasks[id]);
            };
          }
        };
        // Collect array of projects
        var projectsSrc = this.collectProjectOFTasks(sortedTasksSrc,1);
        var projectsDst = this.collectProjectOFTasks(sortedTasksDst,-1);
        // Generate two revisions
        var revisionsArray = {};
        revisionsArray["src"] = { taskscount:sortedTasksSrc.length, projects:projectsSrc };
        revisionsArray["dst"] = { taskscount:sortedTasksDst.length, projects:projectsDst };
        return revisionsArray;
      }
    },

    // ========================================================================
    // When an application is ready for use
    // ========================================================================
    mounted: function() {
      // -------------------------------------------------------------------------
      // Initialize interface language
      // -------------------------------------------------------------------------
      // Load preferred language from browser's storage
      let languageTag = localStorage.language;
      // If no language is selected
      if ( !(languageTag) ) {
        // Detect language of browser
        let languageString = window.navigator.userLanguage || window.navigator.language;
        languageTag = languageString.split('-')[0];
      };
      // Check that the selected language is available
      for (let i = 0; i < this.languages.length; i++) {
        if ( this.languages[i].languageTag == languageTag ) {
          this.language = languageTag;
        }
      }
      // If the language is not yet defined, use the default language.
      if ( !(this.language) ) { this.language = this.languages[0].languageTag };
      // Load language label's from server
      this.loadLanguageLabels();
      // -------------------------------------------------------------------------
      // Set current date
      // -------------------------------------------------------------------------
      var dat = new Date();
      this.currentDate = dat.yyyymmdd();
      dat.setDate(dat.getDate() + 7); // You must change here, if you want another term in the 'public-7days' mode
      this.horizonDate = dat.yyyymmdd();
      // -------------------------------------------------------------------------
      // Initialize user info
      // -------------------------------------------------------------------------
      this.user = 'stkorn@mail.ru' // !!!!!!!!!!!!!!
      // -------------------------------------------------------------------------
      // Initialize tasks database
      // -------------------------------------------------------------------------
      this.syncTasks(); // trying to full synchronization
      // -------------------------------------------------------------------------
      // Initialize task edit form
      // -------------------------------------------------------------------------
      this.addTaskOfStatus('draft');
    },

    // ========================================================================
    // Various event handlers and subroutines
    // ========================================================================
    methods: {

      // -------------------------------------------------------------------------
      // When a user has selected a language
      // -------------------------------------------------------------------------
      onLanguageChange: function (event) {
        // Save selected language
        localStorage.language = this.language;
        // Load language label's from server
        this.loadLanguageLabels();
      },

      // -------------------------------------------------------------------------
      // Load labels of selected language from server (or from cache via service-worker)
      // -------------------------------------------------------------------------
      loadLanguageLabels: function () {
        // Load labels from server
        axios
          .get('/static/languages/'+this.language+'.json?'+version)
          .then(function(response) {
            app.labels = response.data; // Load labels
            document.title = app.labels.titleWebPage; // Update page title
            document.querySelector('meta[name="description"]').setAttribute("content", app.labels.descriptionWebPage); // Update page description
          })
          .catch(function(error) { console.log(error) });
        // Set HTML-document language
        document.documentElement.setAttribute('lang',this.language);
        // Initialize hyphenation library
        Hyphenopoly.hyphenators[ this.language ].then( function(value) {
          Hyphenator = value;
        });
      },

      // -------------------------------------------------------------------------
      // If language is selected and hyphenator is loaded: add invisible hyphens to string
      // else - return original string.
      // -------------------------------------------------------------------------
      tryHyphenator: function(str) {
        if (Hyphenator) {
          return Hyphenator(str);
        } else {
          return str;
        }
      },

      // -------------------------------------------------------------------------
      // Generate list of n columns of projects for current tasks with its weights
      // [
      //   [ {name:project1, items:[task1,task2,..]}, {name:project2, items:[task1,task2,..]}, ... ],
      //   [ {name:project3, items:[task1,task2,..]}, {name:project4, items:[task1,task2,..]}, ... ],
      //   ...
      // ]
      // -------------------------------------------------------------------------
      projectsTasks: function(n) {
        // Generate numeric array of tasks, that can be seen
        var sortedTasks = [], id;
        for (id in this.tasks) {
          if ( this.canSeeTask(this.tasks[id]) ) sortedTasks.push(this.tasks[id]);
        };
        // Collect array of projects
        var arrOfProjects = this.collectProjectOFTasks(sortedTasks,1);
        // Split array of project on 5 parts
        return splitByNames( arrOfProjects, n );
      },

      // -------------------------------------------------------------------------
      // Generate list of projects for incoming tasks array
      // IN: unsorted array of tasks [task1, task2, ...]
      //     order of project names: 1 (alphabetical), -1 (reverse)
      // OUT: sorted array of projects with sorted tasks inside:
      // [ {name:project1, items:[task1,task2,..]}, {name:project2, items:[task1,task2,..]}, ... ]
      // -------------------------------------------------------------------------
      collectProjectOFTasks: function(tsks,order) {
        // Sorts it in the order, in which tasks within the project should be displayed
        tsks.sort(sortProjectTasks);
        // Generate array of projects with its tasks:
        // { project1:[task1,task2,..], project2:[task1,task2,..], ... }
        var listProjects = {}, l = tsks.length, i;
        for( i=0; i<l; i++) {
          var projectName = tsks[i].Project;
          if (!(projectName)) projectName = this.labels.labelEmptyProject;
          if (!(listProjects[projectName])) { listProjects[projectName] = [] };
          listProjects[projectName].push(tsks[i]);
        };
        // Convert a set of properties to the processed format of an array of objects
        // [ {name:project1, items:[task1,task2,..]}, {name:project2, items:[task1,task2,..]}, ... ]
        var arrOfProjects = [];
        for (projectName in listProjects) {
          var project = {};
          project.name = projectName;
          project.items = listProjects[projectName];
          arrOfProjects.push(project);
        };
        // Sort projects by it names
        arrOfProjects.sort( function(a,b) {
          if (a.name < b.name) return order*(-1);
          if (a.name > b.name) return order;
          return 0;
        });
        // Return array of projects
        return arrOfProjects;
      },

      // -------------------------------------------------------------------------
      // Send to server latest timestamp of tasks, synchronized from server (or null for full-synchroniztion).
      // Send to server all tasks without timestamps: created or edited on client-side.
      // Server updates task information received from client.
      // Then receives from server list of all tasks, modified since sended timestamp.
      // Visually highlights new tasks, received from the server.
      // -------------------------------------------------------------------------
      syncTasks: function (fromTimestamp) {
        // Generate numeric array of tasks, that can be seen
        var preparedTasks = [], id;
        for (id in this.tasksToUpdate) {
          preparedTasks.push(this.tasksToUpdate[id]);
        };
        var preparedTodays = [];
        for (id in this.todaysToUpdate) {
          preparedTodays.push( { ID: id, 
                                 Email: this.user,
                                 Delimiter: this.todaysToUpdate[id].Delimiter,
                                 Index: this.todaysToUpdate[id].Index,
                                 Todaystamp: this.todaysToUpdate[id].Todaystamp} );
        };
        // Prepare request variable and status label
        var jsonRequest = { UpdatedOnClientTasks: preparedTasks, 
                            UpdatedOnClientTodays: preparedTodays };
        if (fromTimestamp) { 
          this.status = "TryPartSync";
          jsonRequest.FromTimestamp = fromTimestamp;
        } else {
          this.status = "TryFullSync"
        };
        // Execute ajax request
        axios
          .post('/SyncTasks', jsonRequest)
          .then(function(response)  {
            // In case of full synchronization, replace the list of tasks with loaded from the server
            if (app.status == "TryFullSync") app.tasks = { };
            // Always replace the list of todays with loaded from the server
            app.todays = [ ];
            // Process tasks, returned by the server
            if (response.data.UpdatedOnServerTasks) {
              var l=response.data.UpdatedOnServerTasks.length, i;
              for (i=0; i<l; i++) {
                //app.tasks[response.data.UpdatedOnServerTasks[i].ID] = copyTask( response.data.UpdatedOnServerTasks[i], app.tasks[response.data.UpdatedOnServerTasks[i].ID] );
                Vue.set(app.tasks, response.data.UpdatedOnServerTasks[i].ID, response.data.UpdatedOnServerTasks[i]);
              };
            };
            // Process todays, returned by the server
            if (response.data.FullActualTodays) {
              l=response.data.FullActualTodays.length;
              for (i=0; i<l; i++) {
                if (response.data.FullActualTodays[i].Delimiter) {
                  app.todays.push( response.data.FullActualTodays[i] );
                } else {
                  // !!!!! Can be not found?
                  app.tasks[response.data.FullActualTodays[i].ID].Index = response.data.FullActualTodays[i].Index;
                  app.tasks[response.data.FullActualTodays[i].ID].Todaystamp = response.data.FullActualTodays[i].Todaystamp;
                  app.todays.push( app.tasks[response.data.FullActualTodays[i].ID] );
                }
              };
            };
            // Calculate the server timestamp of the youngest task
            app.lastSynchronizedTimestamp = undefined;
            for (var id in app.tasks) {
              if ( (!(app.lastSynchronizedTimestamp)) || (app.tasks[id].Timestamp > app.lastSynchronizedTimestamp) ) {
                app.lastSynchronizedTimestamp=app.tasks[id].Timestamp;
              };
              if ( ((app.tasks[id].Todaystamp)) && (app.tasks[id].Todaystamp > app.lastSynchronizedTimestamp) ) {
                app.lastSynchronizedTimestamp=app.tasks[id].Todaystamp;
              };
            };
            // Finish sync
            app.status = "SyncDone";
            app.tasksToUpdate = { };
            app.todaysToUpdate = { };
            app.updateStatistic();
          })
          .catch(function(error)  {
            console.log(error);
            app.status = "SyncFailed";
          });
      },

      // -------------------------------------------------------------------------
      // Generate string with status of tasks and todays to be synchronized
      // Update all statistics counters
      // -------------------------------------------------------------------------
      updateStatistic: function() {
        // Generate string with status of tasks and todays to be synchronized
        var countTasksToUpdate = 0;
        for (id in this.tasksToUpdate) { countTasksToUpdate++; }
        var countTodaysToUpdate = 0;
        for (id in this.todaysToUpdate) { countTodaysToUpdate++; }
        if ( (countTasksToUpdate>0) || (countTodaysToUpdate>0) ) {
          this.awaitingSync = this.labels.statusWaitForSync + countTasksToUpdate + ' + ' + countTodaysToUpdate + this.labels.statusWaitForSyncToday;
        } else {
          this.awaitingSync = '';
        };
        // Calculate maximum of revision number
        var maxRevision = 0;
        // Update all statistics counters
        // Clear all statistic counters
        this.statistic = { TodayTasks: this.todays.length,
                           AssignedToMeTasks: 0,
                           MyTasks: 0,
                           MyTasksNotPrivate: 0,
                           MyTasksPrivate: 0,
                           AssignedByMeTasks: 0,
                           AssignedByMeOverdueTasks: 0,
                           AssignedByMeHaveTimeTasks: 0,
                           AssignedByMeWithoutTimeTasks: 0,
                           DraftTasks: 0,
                           DoneAndVerified: 0,
                           Canceled: 0,
                           Moved: 0 };
        // Enumerate all tasks
        for (var id in this.tasks) {
          // Check maximum of revision number
          if ( (!(this.tasks[id].Assigned)) || (this.tasks[id].Assigned!=this.user) ) {
            if (this.tasks[id].Revision > maxRevision) maxRevision=this.tasks[id].Revision;
          };
          // Increase counters
          if (this.tasks[id].Status=='draft') {
            this.statistic.DraftTasks++;
          } else if (this.tasks[id].Status=='done-and-verified') {
            this.statistic.DoneAndVerified++;
          } else if (this.tasks[id].Status=='canceled') {
            this.statistic.Canceled++;
          } else if (this.tasks[id].Status=='moved') {
            this.statistic.Moved++;
          } else if (this.tasks[id].Assigned==this.user) {
            this.statistic.AssignedToMeTasks++;
          } else if ( (this.tasks[id].Assigned) && (this.tasks[id].Assigned!=this.user) ) {
            this.statistic.AssignedByMeTasks++;
            if (!(this.tasks[id].Term)) {
              this.statistic.AssignedByMeWithoutTimeTasks++;
            } else if (this.tasks[id].Term<this.currentDate) {
              this.statistic.AssignedByMeOverdueTasks++;
            } else {
              this.statistic.AssignedByMeHaveTimeTasks++;
            }
          } else {
            this.statistic.MyTasks++;
            if (this.tasks[id].Private) {
              this.statistic.MyTasksPrivate++;
            } else {
              this.statistic.MyTasksNotPrivate++;
            }
          }
        };
        // Update maximum of revision number
        this.currentRevision = maxRevision;
        if (this.targetRevision==0) this.targetRevision=this.currentRevision;
        if ((this.task.Revision==0) && !(this.task.ID)) this.task.Revision=this.targetRevision;
      },

      // -------------------------------------------------------------------------
      // Determines whether the task can be displayed on the screen, 
      // depending on the display mode selected by the user.
      // IN: tsk : task structure
      // OUT: boolean: task can be displayed
      // -------------------------------------------------------------------------
      canSeeTask: function (tsk) {
        if (this.viewType != 'all') {
          if ( (tsk.Status == 'moved') || (tsk.Status == 'canceled') || (tsk.Status == 'done-and-verified') ) return false;
        };
        if ((this.viewType == 'only-public') || (this.viewType == 'public-7days')) {
          if (tsk.Private) return false;
          if (tsk.Status=='draft') return false;
        }
        if (this.viewType=='public-7days') {
          if (!(tsk.Term)) return false;
          if (tsk.Term>this.horizonDate) return false;
        }
        if (this.viewFilter > '') {
          if ( (!(tsk.Content) || (tsk.Content.toLowerCase().indexOf(this.viewFilter.toLowerCase()) == -1)) && 
               (!(tsk.Project) || (tsk.Project.toLowerCase().indexOf(this.viewFilter.toLowerCase()) == -1)) && 
               (!(tsk.Assigned) || (tsk.Assigned.toLowerCase().indexOf(this.viewFilter.toLowerCase()) == -1)) ) return false;
        }
        return true;
      },

      // -------------------------------------------------------------------------
      // Determines whether the today's task can be displayed on the screen, 
      // depending on filter, entered by the user.
      // IN: td : today's structure
      // OUT: boolean: today's can be displayed
      // -------------------------------------------------------------------------
      canSeeToday: function (tsk) {
        if (this.viewFilter > '') {
          if ( (!(tsk.Content) || (tsk.Content.toLowerCase().indexOf(this.viewFilter.toLowerCase()) == -1)) && 
               (!(tsk.Project) || (tsk.Project.toLowerCase().indexOf(this.viewFilter.toLowerCase()) == -1)) && 
               (!(tsk.Assigned) || (tsk.Assigned.toLowerCase().indexOf(this.viewFilter.toLowerCase()) == -1)) ) return false;
        }
        return true;
      },

      // -------------------------------------------------------------------------
      // Get hint of current sync status
      // IN: status : string
      // OUT hint : string
      // -------------------------------------------------------------------------
      getHintOfSyncStatus: function (status) {
        switch (status) {
          case 'Loading' : return this.labels.statusLoading
          case 'TryFullSync' : return this.labels.statusTryFullSync
          case 'TryPartSync' : return this.labels.statusTryPartSync
          case 'SyncDone' : return this.labels.statusSyncDone
          case 'SyncFailed' : return this.labels.statusSyncFailed
          default : return ''
        }
      },

      // -------------------------------------------------------------------------
      // Get hint of current task status (if necessary)
      // IN: task.status : string
      // OUT hint : string
      // -------------------------------------------------------------------------
      getHintOfTaskStatus: function (status) {
        switch (status) {
          case 'need-help' : return this.labels.optionNeedHelp
          case 'moved' : return this.labels.optionMoved
          case 'canceled' : return this.labels.optionCanceled
          case 'done-but-not-verified' : return this.labels.optionDoneButNotVerified
          case 'done-and-verified' : return this.labels.optionDoneAndVerified
          default : return ''
        }
      },

      // -------------------------------------------------------------------------
      // Get hint of current task icon (if exist)
      // IN: task.icon : string
      // OUT hint : string
      // -------------------------------------------------------------------------
      getHintOfTaskIcon: function(icon) {
        switch (icon) {
          case 'wait' : return this.labels.optionWait
          case 'think' : return this.labels.optionThink
          case 'remid' : return this.labels.optionRemind
          case 'call' : return this.labels.optionCall
          case 'force' : return this.labels.optionForce
          case 'mail' : return this.labels.optionMail
          case 'prepare' : return this.labels.optionPrepare
          case 'manage' : return this.labels.optionManage
          case 'meet' : return this.labels.optionMeet
          case 'visit' : return this.labels.optionVisit
          case 'make' : return this.labels.optionMake
          case 'journey' : return this.labels.optionJourney
          default : return ''
        }
      },

      // -------------------------------------------------------------------------
      // Get hint of current task term (if exist)
      // IN: task.term : string
      // OUT hint : string
      // -------------------------------------------------------------------------
      getHintOfTaskTerm: function(term) {
        // If term is empty - then exit
        if (!(term)) return '';
        // Parse term of current task
        var termArray = term.split("-");
        var termDate = new Date(termArray[0], termArray[1] - 1, termArray[2]);
        var currentDate = new Date();
        currentDate.setHours(0,0,0,0);
        var oneDay = 24*60*60*1000;
        // Term is overdue
        if (termDate < currentDate) {
          var diffDays = Math.round(Math.abs((currentDate.getTime() - termDate.getTime())/(oneDay)));
          return this.labels.hintOverduTerm + diffDays;
        };
        // Term is not overdue
        if (termDate > currentDate) {
          var diffDays = Math.round(Math.abs((termDate.getTime() - currentDate.getTime())/(oneDay)));
          return this.labels.hintBeforeTerm + diffDays;
        };
        // Term today
        if (termDate.getTime() == currentDate.getTime()) {
          return this.labels.hintTodayTerm;
        };
        // Nothing matched
        return '';
      },

      // -------------------------------------------------------------------------
      // Starts editing on the task form an existing task (after the user clicks on it)
      // IN: task : task
      // OUT --
      // -------------------------------------------------------------------------
      editTask: function (tsk) {
        this.task = copyTask( tsk, undefined );
        // check current task in today's tasks array
        this.today = false;
        var i, l=this.todays.length;
        for (i=0; i<l; i++) {
          if (this.todays[i].ID == tsk.ID) this.today=true;
        };
        // if current mode = 'revisions': move task to new revision
        if (this.viewMode=='revisions') {
          this.task.Revision = this.targetRevision;
        }
        // remove task from draft on second edit
        if (this.task.Status=='draft') this.task.Status='created';
        // start edit task content
        this.$refs.content.focus();
      },

      // -------------------------------------------------------------------------
      // Starts adding on the task form a new task with a given status
      // IN: status : string
      // OUT --
      // -------------------------------------------------------------------------
      addTaskOfStatus: function (status) {
        this.task = {};
        this.task.Icon = '';
        this.task.Revision = this.currentRevision;
        this.today = false;
        this.task.Status = status;
        this.$refs.content.focus();
      },

      // -------------------------------------------------------------------------
      // Starts adding on the task form a new task in a given poroject
      // IN: project : string
      // OUT --
      // -------------------------------------------------------------------------
      addTaskToProject: function (project) {
        this.task = {};
        this.task.Icon = '';
        this.task.Revision = this.currentRevision;
        this.today = false;
        this.task.Status = 'created';
        if (project==this.labels.labelEmptyProject) {
          this.task.Project = '';
        } else {
          this.task.Project = project;
        };
        this.$refs.content.focus();
      },

      // -------------------------------------------------------------------------
      // Starts adding on the task form a new task in a given revision
      // IN: revision : integer
      // OUT --
      // -------------------------------------------------------------------------
      addTaskOfRevision: function (revision) {
        this.task = {};
        this.task.Icon = '';
        this.task.Revision = revision;
        this.today = false;
        this.task.Status = 'created';
        this.$refs.content.focus();
      },

      // -------------------------------------------------------------------------
      // Create new delimiter and insert it after selected task
      // IN: task, from today's list, after which you need to insert a delimiter
      // -------------------------------------------------------------------------
      addDelimiter: function (tsk) {
        var tskIndex = this.todays.indexOf(tsk);
        // Prepare new delimiter
        var td = {};
        td.Delimiter = true;
        td.ID = uuidv4();
        if (tskIndex == (this.todays.length-1)) {
          td.Index = tsk.Index+1000;
        } else {
          td.Index = (tsk.Index + this.todays[tskIndex+1].Index )/2;
        }
        // Insert prepared delimiter in todays
        this.todays.splice( tskIndex+1, 0, td);
        // Save for update
        if (!(this.todaysToUpdate[td.ID])) this.todaysToUpdate[td.ID]={};
        this.todaysToUpdate[td.ID].Index = td.Index;
        this.todaysToUpdate[td.ID].Delimiter = td.Delimiter;
        // Refresh count of awaiting synchronization tasks and statistic counters
        this.updateStatistic();
        // Trying to partially synchronize tasks if this is not happening now
        if ( (Object.keys(this.todaysToUpdate).length>0) || (Object.keys(this.tasksToUpdate).length>0) ) {
          if ((this.status != "TryPartSync") && (this.status != "TryFullSync")) {
            this.syncTasks(this.lastSynchronizedTimestamp);
          };
        };
      },

      // -------------------------------------------------------------------------
      // Remove existing delimiter from today's list
      // IN: task structure of removed delimiter
      // -------------------------------------------------------------------------
      removeDelimiter: function (tsk) {
        // Save for update
        this.todaysToUpdate[tsk.ID] = { Index: -1, Delimiter: true, Todaystamp: tsk.Todaystamp };
        // Remove delimiter
        this.todays.splice(this.todays.indexOf(tsk),1);
        // Refresh count of awaiting synchronization tasks and statistic counters
        this.updateStatistic();
        // Trying to partially synchronize tasks if this is not happening now
        if ( (Object.keys(this.todaysToUpdate).length>0) || (Object.keys(this.tasksToUpdate).length>0) ) {
          if ((this.status != "TryPartSync") && (this.status != "TryFullSync")) {
            this.syncTasks(this.lastSynchronizedTimestamp);
          };
        };
      },

      // -------------------------------------------------------------------------
      // Saves the edited task from the input fields on the task form to the working task array
      // -------------------------------------------------------------------------
      saveTask: function () {
        // Prevent empty tasks
        if ( !(this.task.Content) || !(this.task.Content.trim()) ) {
          this.$refs.content.focus();
          alert(this.labels.alertEmptyContent);
          return;
        }
        // Check is task really changed
        var taskChanged=true;
        // Save task
        if (this.task.ID) {
          // compare tasks
          taskChanged = !(equalsTasks( this.task, this.tasks[this.task.ID] ));
          // update existing task
          //copyTask( this.task, this.tasks[this.task.ID] );
          Vue.set(this.tasks, this.task.ID, this.task);
        } else {
          // add new task
          this.task.ID = uuidv4();
          this.task.Email = this.user;
          //this.task.Revision = 0;
          this.task.Timestamp = undefined;
          this.task.ExternalID = '';
          this.task.ParentID = ''; // !!!!!!!!!!!!!
          //this.tasks[this.task.ID] = this.task;
          Vue.set(this.tasks, this.task.ID, this.task);
        };
        // If task was really changed
        if (taskChanged) {
          // If task is not just in update list - add it to update list
          this.tasksToUpdate[this.task.ID] = this.tasks[this.task.ID];
        }
        // Scan today's tasks list for this task
        var idx=-1, i, l=this.todays.length;
        for (i=0; i<l; i++) {
          if (this.todays[i].ID == this.task.ID) idx=i;
        };
        // Check if edited includes or excludes in today's task list
        if (this.today) {
          // not found -> add it
          if (idx<0) {
            var index;
            if (this.todays.length==0) {
              index = 1000;
            } else {
              index = this.todays[this.todays.length-1].Index + 1000;
            };
            this.todays.push( this.tasks[this.task.ID] );
            this.todays[this.todays.length-1].Index = index;
            // save for update                    
            if (!(this.todaysToUpdate[this.task.ID])) this.todaysToUpdate[this.task.ID]={};
            this.todaysToUpdate[this.task.ID].Index = index;
            this.todaysToUpdate[this.task.ID].Delimiter = false;
          };
        } else {
          // found -> remove it
          if (idx>=0) {
            // save for update
            this.todaysToUpdate[this.task.ID] = { Index: -1, Delimiter: false, Todaystamp: this.todays[idx].Todaystamp };
            // remove
            this.todays.splice(idx,1);
          };
        };
        // Refresh count of awaiting synchronization tasks and statistic counters
        this.updateStatistic();
        // Trying to partially synchronize tasks if this is not happening now
        if ( (Object.keys(this.todaysToUpdate).length>0) || (Object.keys(this.tasksToUpdate).length>0) ) {
          if ((this.status != "TryPartSync") && (this.status != "TryFullSync")) {
            this.syncTasks(this.lastSynchronizedTimestamp);
          };
        };
        // Preparing to add new draft task
        if (this.viewMode=='revisions') { 
          this.addTaskOfStatus('created'); 
        } else {
          this.addTaskOfStatus('draft'); 
        };
      },

      // -------------------------------------------------------------------------
      // Return true if task with ID=taskID found in today's tasks list. Else return false.
      // -------------------------------------------------------------------------
      taskInTodayList: function(tskID) {
        var i, l=this.todays.length;
        for (var i=0; i<l; i++) {
          if (this.todays[i].ID == tskID) return true;
        }
        return false;
      },

      // -------------------------------------------------------------------------
      // Event when today's tasks list changed (added, moved or removed task)
      // -------------------------------------------------------------------------
      onTodayTasksChanged: function(evt) {
        // Check new position of changed element
        var newIndex;
        if (evt.added) {
          newIndex = evt.added.newIndex;
        } else if (evt.moved) {
          newIndex = evt.moved.newIndex;
        } else {
          return;
        };
        // Compare new position of changed element with positions of other elements
        if ((newIndex==0) && (this.todays.length==1)) { // single element
          this.todays[newIndex].Index = 1000;
        } else if (newIndex==(this.todays.length-1)) { // last element
          this.todays[newIndex].Index = this.todays[newIndex-1].Index + 1000;
        } else if (newIndex==0) { // first element
          this.todays[newIndex].Index = this.todays[newIndex+1].Index / 2;
        } else { // between two elements
          this.todays[newIndex].Index = (this.todays[newIndex+1].Index + this.todays[newIndex-1].Index) / 2;
        };
        // If task is not just in today's tasks update list - add it to update list
        this.todaysToUpdate[this.todays[newIndex].ID] = { Index: this.todays[newIndex].Index,
                                                          Delimiter: this.todays[newIndex].Delimiter,
                                                          Todaystamp: this.todays[newIndex].Todaystamp };
        // Refresh count of awaiting synchronization tasks and statistic counters
        this.updateStatistic();
        // Trying to partially synchronize tasks if this is not happening now
        if ( (Object.keys(this.todaysToUpdate).length>0) || (Object.keys(this.tasksToUpdate).length>0) ) {
          if ((this.status != "TryPartSync") && (this.status != "TryFullSync")) {
            this.syncTasks(this.lastSynchronizedTimestamp);
          };
        };
      },

      // -------------------------------------------------------------------------
      // Clears task filter by text
      // -------------------------------------------------------------------------
      clearFilter: function() {
        this.viewFilter = '';
      },

      // -------------------------------------------------------------------------
      // Remove done, canceled and moved tasks form today's tasks list
      // -------------------------------------------------------------------------
      clearDoneCanceledMovedTodays: function() {
        // Enumerate today's tasks
        for (var i=0; i<this.todays.length; i++) {
          if ( (this.todays[i].Status == 'done-and-verified') ||
               (this.todays[i].Status == 'canceled') ||
               (this.todays[i].Status == 'moved') ) {
                  // save for update
                  this.todaysToUpdate[this.todays[i].ID] = { Index: -1, Delimiter: this.todays[i].Delimiter, Todaystamp: this.todays[i].Todaystamp };
                  // remove
                  this.todays.splice(i,1);
                  // goto next step
                  i--;
               }
        };
        // Refresh count of awaiting synchronization tasks and statistic counters
        this.updateStatistic();
        if ( (Object.keys(this.todaysToUpdate).length>0) || (Object.keys(this.tasksToUpdate).length>0) ) {
          // Trying to partially synchronize tasks if this is not happening now
          if ((this.status != "TryPartSync") && (this.status != "TryFullSync")) {
            this.syncTasks(this.lastSynchronizedTimestamp);
          };
        };
      },

      // -------------------------------------------------------------------------
      // Remove all tasks form today's tasks list
      // -------------------------------------------------------------------------
      clearAllTodays: function() {
        // Enumerate today's tasks
        for (var i=this.todays.length-1; i>=0; i--) {
          // save for update
          this.todaysToUpdate[this.todays[i].ID] = { Index: -1, Delimiter: this.todays[i].Delimiter, Todaystamp: this.todays[i].Todaystamp };
          // remove
          this.todays.pop();
        };
        // Refresh count of awaiting synchronization tasks and statistic counters
        this.updateStatistic();
        // Trying to partially synchronize tasks if this is not happening now
        if ( (Object.keys(this.todaysToUpdate).length>0) || (Object.keys(this.tasksToUpdate).length>0) ) {
          if ((this.status != "TryPartSync") && (this.status != "TryFullSync")) {
            this.syncTasks(this.lastSynchronizedTimestamp);
          };
        };
      },

      // -------------------------------------------------------------------------
      // Dummy function to replace default actions, eg. forms submit 
      // -------------------------------------------------------------------------
      doNothing: function() {
        return;
      }
    }
  });

}

// -------------------------------------------------------------------------
// Utility function of sorting assignet to me tasks
// -------------------------------------------------------------------------
function sortAssignedToMeTasks(a,b) {
  // First sort by term
  if ((a.Term) && !(b.Term)) return -1;
  if (!(a.Term) && (b.Term)) return 1;
  if (a.Term < b.Term) return -1;
  if (a.Term > b.Term) return 1;
  // Secondly sort by email
  if (a.Email < b.Email) return -1;
  if (a.Email > b.Email) return 1;
  // Third sort by timestamp
  if (a.Timestamp < b.Timestamp) return -1;
  if (a.Timestamp > b.Timestamp) return 1;
  return 0;
}
// -------------------------------------------------------------------------
// Utility function of sorting my own tasks
// -------------------------------------------------------------------------
function sortMyTasks(a,b) {
  // First sort by term
  if ((a.Term) && !(b.Term)) return -1;
  if (!(a.Term) && (b.Term)) return 1;
  if (a.Term < b.Term) return -1;
  if (a.Term > b.Term) return 1;
  // Secondly sort by icon
  if (a.Icon < b.Icon) return -1;
  if (a.Icon > b.Icon) return 1;
  // Third sort by project
  if (a.Project < b.Project) return -1;
  if (a.Project > b.Project) return 1;
  // Fourth sort by timestamp
  if (a.Timestamp < b.Timestamp) return -1;
  if (a.Timestamp > b.Timestamp) return 1;
  return 0;
}
// -------------------------------------------------------------------------
// Utility function of sorting assignet by me tasks
// -------------------------------------------------------------------------
function sortAssignedByMeTasks(a,b) {
  // First sort by term
  if ((a.Term) && !(b.Term)) return -1;
  if (!(a.Term) && (b.Term)) return 1;
  if (a.Term < b.Term) return -1;
  if (a.Term > b.Term) return 1;
  // Secondly sort by employee
  if (a.Assigned < b.Assigned) return -1;
  if (a.Assigned > b.Assigned) return 1;
  // Third sort by timestamp
  if (a.Timestamp < b.Timestamp) return -1;
  if (a.Timestamp > b.Timestamp) return 1;
  return 0;
}
// -------------------------------------------------------------------------
// Utility function of sorting draft tasks
// -------------------------------------------------------------------------
function sortDraftTasks(a,b) {
  // First sort by timestamp
  if (a.Timestamp < b.Timestamp) return -1;
  if (a.Timestamp > b.Timestamp) return 1;
  return 0;
}
// -------------------------------------------------------------------------
// Utility function of sorting tasks in a project
// -------------------------------------------------------------------------
function sortProjectTasks(a,b) {
  // First sort by term
  if ((a.Term) && !(b.Term)) return -1;
  if (!(a.Term) && (b.Term)) return 1;
  if (a.Term < b.Term) return -1;
  if (a.Term > b.Term) return 1;
  // Secondly sort by employee
  if (a.Assigned < b.Assigned) return -1;
  if (a.Assigned > b.Assigned) return 1;
  // Third sort by icon
  if (a.Icon < b.Icon) return -1;
  if (a.Icon > b.Icon) return 1;
  // Fourth sort by timestamp
  if (a.Timestamp < b.Timestamp) return -1;
  if (a.Timestamp > b.Timestamp) return 1;
  return 0;
}

// -------------------------------------------------------------------------
// Utility function of splitting objects into N parts with the most similar summary subobject's count 
// Attention: this function PRESERVES the original sorting order of the elements in the array.
// IN: arrObj: [ {name:object1, items:[sub1,sub2,..]}, {name:object2, items:[task1,task2,..]}, ... ]
//     N: int
// OUT array of arrays: 
// [ 
//   [ {name:object1, items:[sub1,sub2,..]}, {name:object2, items:[task1,task2,..]}, ... ],
//   [ {name:object3, items:[sub1,sub2,..]}, {name:object4, items:[task1,task2,..]}, ... ],
//   ...
// ]
// -------------------------------------------------------------------------
function splitByNames(arrObj, n)
{
  var splitedArr = []; // multidimensional array for storing separated arrays (result)
  var arrWeights = []; // array for storing array weights
  var i, j; // loop variables

  // Generate columns
  for (i=0; i<n; i++) {
    splitedArr[i]=[];
    arrWeights[i]=0;
  }

  // Calculate the average size of one column
  var l=arrObj.length; // count of objects
  var sum=0; // total weight of objects
  var avg; // average column weight
  for (i=0; i<l; i++) {
    sum = sum + arrObj[i].items.length + 1; // reserve one line for the title of the object
  };
  avg = sum / n;

  // Loop through the elements
  var col = 0; // current column index
  for (i=0; i<l; i++) {

    // If the current column is full, and this isn't last column - then go to next column
    if ((arrWeights[col] >= avg) && (col<(n-1))) col++;
    
    // If, after adding a object, the column still has few elements
    // or this is last column - then add object
    if ( ((arrWeights[col]+arrObj[i].items.length+1)<=avg) || (col==(n-1))) {
      splitedArr[col].push( arrObj[i] );
      arrWeights[col] = arrWeights[col] + arrObj[i].items.length + 1;
      continue;
    }
    
    // If adding another object will overflow the column

    // Calculate the standard deviation if the object appears in the current column
    var remainingWeights=0; // the total weight of the objects remaining for distribution by columns
    var predictedWeights=[]; // predicted column weights
    var addToCurrentSD=0; // standard deviation, provided that the current object is added to the current column
    var addToCurrentAVG=0; //average remaining column weight
    for (j=0; j<col; j++) { predictedWeights[j] = arrWeights[j] };
    predictedWeights[col] = arrWeights[col] + arrObj[i].items.length + 1;
    for (j=(i+1); j<l; j++) { remainingWeights += arrObj[j].items.length + 1 };
    addToCurrentAVG = remainingWeights / (n-col-1);
    for (j=(col+1); j<n; j++) { predictedWeights[j] = addToCurrentAVG };
    for (j=0; j<n; j++) { addToCurrentSD = addToCurrentSD + ((predictedWeights[j]-avg)*(predictedWeights[j]-avg)) };
    addToCurrentSD = Math.sqrt(addToCurrentSD/n);

    // Calculate the standard deviation if the object appears in the next column
    var remainingWeights=0; // the total weight of the objects remaining for distribution by columns
    var predictedWeights=[]; // predicted column weights
    var addToNextSD=0; // standard deviation, provided that the current object is added to the next column
    var addToNextAVG=0; //average remaining column weight
    for (j=0; j<col; j++) { predictedWeights[j] = arrWeights[j] };
    predictedWeights[col] = arrWeights[col];
    for (j=i; j<l; j++) { remainingWeights += arrObj[j].items.length + 1 };
    addToNextAVG = remainingWeights / (n-col-1);
    for (j=(col+1); j<n; j++) { predictedWeights[j] = addToNextAVG };
    for (j=0; j<n; j++) { addToNextSD = addToNextSD + ((predictedWeights[j]-avg)*(predictedWeights[j]-avg)) };
    addToNextSD = Math.sqrt(addToNextSD/n);

    // Choose the option with the minimum standard deviation
    if (addToCurrentSD>addToNextSD) { 
      col++; // Go to next column
      avg = addToNextAVG;
    } else {
      avg = addToCurrentAVG;
    }

    // Add current object to current column
    splitedArr[col].push( arrObj[i] );
    arrWeights[col] = arrWeights[col] + arrObj[i].items.length + 1;
  };

  // Return array of splitted parts     
  return splitedArr;
};

// -------------------------------------------------------------------------
// Utility function of splitting objects into N parts with the most similar summary subobject's count
// Attention: this function CHANGES the initial sorting order of the elements in the array.
// IN: arrObj: [ {name:object1, items:[sub1,sub2,..]}, {name:object2, items:[task1,task2,..]}, ... ]
//     N: int
// OUT array of arrays: 
// [ 
//   [ {name:object1, items:[sub1,sub2,..]}, {name:object2, items:[task1,task2,..]}, ... ],
//   [ {name:object3, items:[sub1,sub2,..]}, {name:object4, items:[task1,task2,..]}, ... ],
//   ...
// ]
// -------------------------------------------------------------------------
function splitByWeights(arrObj, n)
{
  var splitedArr = []; // multidimensional array for storing separated arrays (result)
  var arrWeights = []; // array for storing array weights

  // Sorting the elements of the input array by weights by decreasing the number of sub-objects
  arrObj.sort( function(a,b) {
    if (a.items.length<b.items.length) return 1;
    if (a.items.length>b.items.length) return -1;
    return 0;
  } );

  // We decompose the first elements with the largest weight from the input array 
  // into newly created arrays (one by one) and write the weight into an array of weights
  for(var i=0; i<n; i++)
	{
    splitedArr[i]=[];
    splitedArr[i].push( arrObj[i] );
    arrWeights[i]=arrObj[i].items.length;
	};

  // We decompose the remaining elements:
  // put the current element in the array in which the total weight is the least
  for(var i=n; i<arrObj.length; i++)
  {
    // Search for the index of the part with the lowest weight
    var idxOfMinColumn = 0;
    for (var k=1; k<n; k++) {
      if (arrWeights[k]<arrWeights[idxOfMinColumn]) idxOfMinColumn=k;
    }
    // Add new element to selected part
    splitedArr[idxOfMinColumn].push( arrObj[i] );
    arrWeights[idxOfMinColumn]+=arrObj[i].items.length;
  };

  // Return array of splitted parts     
  return splitedArr;
};

// -------------------------------------------------------------------------
// Utility function of copying task structure with all propetries
// IN: from - souce task structure (passed by reference)
// IN: to - destination task structure (passed by reference, might be undefined)
//     if to==undefined, creates in memory a new structure for the task and returns a link to it.
// OUT: return link to destination task ("to" or new created)
// Usage: to=copyTask(from,to)
// -------------------------------------------------------------------------
function copyTask(from, to) {
  if (to===undefined) to = {};
  to.ID = from.ID;
  to.Email = from.Email;
  to.Content = from.Content;
  to.Private = from.Private;
  to.Status = from.Status;
  to.Revision = from.Revision;
  to.Timestamp = from.Timestamp;
  to.ExternalID = from.ExternalID;
  to.Assigned = from.Assigned;
  to.Term = from.Term;
  to.Icon = from.Icon;
  to.Project = from.Project;
  to.Protocol = from.Protocol;
  to.ParentID = from.ParentID;
  return to;
}

// -------------------------------------------------------------------------
// Utility function ofcomparing tasks structures with all propetries
// IN: task1, task2 - task structures
// OUT: boolean:
// -------------------------------------------------------------------------
function equalsTasks(task1, task2) {
  if ((task1) && (!(task2))) return false;
  if ((!(task1)) && (task2)) return false;
  if (task1.ID != task2.ID) return false;
  if (task1.Email != task2.Email) return false;
  if (task1.Content != task2.Content) return false;
  if (task1.Private != task2.Private) return false;
  if (task1.Status != task2.Status) return false;
  if (task1.Revision != task2.Revision) return false;
  //if (task1.Timestamp != task2.Timestamp) return false;
  if (task1.ExternalID != task2.ExternalID) return false;
  if (task1.Assigned != task2.Assigned) return false;
  if (task1.Term != task2.Term) return false;
  if (task1.Icon != task2.Icon) return false;
  if (task1.Project != task2.Project) return false;
  if (task1.Protocol != task2.Protocol) return false;
  if (task1.ParentID != task2.ParentID) return false;
  return true;
};

// -------------------------------------------------------------------------
// Utility function of converting Date to string 'YYYY-MM-DD' format
// Usage: Date().yyyymmdd()
// -------------------------------------------------------------------------
Date.prototype.yyyymmdd = function() {
  var mm = this.getMonth() + 1; // getMonth() is zero-based
  var dd = this.getDate();

  return [this.getFullYear(), '-',
          (mm>9 ? '' : '0') + mm, '-',
          (dd>9 ? '' : '0') + dd
         ].join('');
};



