const CACHE = 'todo-cache-storage';

// ========================================================================
// Function: remove version parameter from URLs of static resources
// ========================================================================
function removeVersion(request) {
    var init = {
        method: request.method,
        headers: request.headers,
        body: request.body,
        //mode: request.mode,
        credentials: request.credentials,
        cache: request.cache,
        redirect: request.redirect,
        referrer: request.referrer
    };
    var URL;
    if (request.url.includes('/static/')) {
        URL = request.url.split('?')[0]
    } else {
        URL = request.url
    };
    return new Request(URL,init);
}

// ========================================================================
// On service-worker installation try to cache all static resources
// ========================================================================
self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(CACHE).then( function(cache) {
            // Сache all static resources: this can be useful for caching on the first opening
            // all icons, all languages (even never used before)
            cache.addAll([
                '/',
                '/static/icons/call.svg',
                '/static/icons/clock.svg',
                '/static/icons/delete.svg',
                '/static/icons/delimiter.svg',
                '/static/icons/force.svg',
                '/static/icons/journey.svg',
                '/static/icons/mail.svg',
                '/static/icons/make.svg',
                '/static/icons/manage.svg',
                '/static/icons/meet.svg',
                '/static/icons/prepare.svg',
                '/static/icons/private.svg',
                '/static/icons/remind.svg',
                '/static/icons/search.svg',
                '/static/icons/think.svg',
                '/static/icons/visit.svg',
                '/static/icons/wait.svg',
                '/static/languages/en.json',
                '/static/languages/ru.json',
                '/static/libs/axios.min.js',
                '/static/libs/es6-promise.auto.min.js',
                '/static/libs/vue.js',
                '/static/libs/uuidv4.min.js',
                '/static/main.js',
                '/static/main.css'
            ])
        })
    )
});

// ========================================================================
// Check object in cache. If found - return it, then update cache in backgroud.
// If not found - fetch object from server, then update cache and return fetched object.
// ========================================================================
function cacheElseNetwork (event) {

    // Opens the specified cache storage
    return caches.open(CACHE).then( function(cache) {

        // Check if requested resource (without version numbers) exists in cache storage
        return cache.match(removeVersion(event.request)).then( function(matching) {

            // Local function: fetch and update cache
            function fetchAndCache () {
                // Try to fetch object from network
                return fetch(event.request).then( function(response) {
                    // Update cache
                    cache.put(removeVersion(event.request), response.clone());
                    // Return fetched object
                    return response;
                });
            }

            // If not exist in cache, fetch and cache it.
            if (!matching) { return fetchAndCache(); }
  
            // If exists in cache:
            // start updating cache in background.
            fetchAndCache();
            // return from cache
            return matching;

        })

    })

};

// ========================================================================
// At the "fetch" event, use the cache, and only then update it with data from the server
// ========================================================================
self.addEventListener('fetch', function(event) {

    // Don't cache POST requests
    if (event.request.method !== 'GET') {
        return;
    };

    // Do not cache everything except HTTP and HTTPS requests (e.g. installed google chrome plugins)
    if (event.request.url.substring(0,4).toUpperCase() !== 'HTTP') {
        return;
    };

    // respondWith() is sort of a contract between browser and service-worker, which enables 
    // to get a response from service-worker and returns it to the browser as a response to the original request.
    // Without respondWith(), there seems no verb in the event handler what to do with the retrieved response.
    event.respondWith( cacheElseNetwork(event) );

});