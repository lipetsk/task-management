# Task-management environment

## What it is?

Task management environment for people and organizations. The server side is written in "Go" language. It provides registration and operation of clients in a web application using the http protocol. It is planned to release applications for iOS and Android in separate projects.

## Where can I try it?

The public user version will be placed after development instead of the current portal https://todo.works. The version for the state authorities of the Russian Federation will be placed on https://исгу.рф.

## How to install it?

1. Copy files:

* Create clone of this repository or 
* copy all files and directories from here, except those whose names begin with a dot.

2. Install required golang packages for all users:

    ```
    user$ go get github.com/lib/pq
    user$ go install github.com/lib/pq
    user$ go get github.com/satori/go.uuid
    user$ go install github.com/satori/go.uuid
    user$ sudo bash
    root# mv /home/user/go/src/github.com/* /usr/local/go/src/github.com/
    root# rm -rf /home/user/go
    ```

3. Build project: `go build`

4. Set environment variables.

    In Windows you must create `environment.bat`

    ```
    SET LISTEN_PORT=port on which the web-server listens HTTP-requests
    SET DATABASE_HOST=ip address of postgresql database server
    SET DATABASE_PORT=port number on postgresql database server 
    SET DATABASE_USER=login on postgresql database server (no SSL-connection used)
    SET DATABASE_PASSWORD=password on postgresql database server
    SET DATABASE_NAME=database name on postgresql database server
    ```

    Note: if special characters are used in the password, they must be converted to a URL format with double `%` characters. For example: if you use the password `pa$$w*rd` then you should set: `SET DATABASE_PASSWORD=pa%%24%%24w%%2Ard` in Windows or `Environment=DATABASE_PASSWORD=pa%%24%%24w%%2Ard` in Linux.

    In Linux you can add these variables in a shell-script, or add it to `/etc/systemd/system/task-management.service` file, if you want to run this web project as a service:

    ```
    [Unit]
    Description=Task management environment
    After=network.target

    [Service]
    Type=simple
    User=task-management
    ExecStart=/home/task-management/task-management
    WorkingDirectory=/home/task-management
    Restart=on-abort
    Environment=LISTEN_PORT=
    Environment=DATABASE_HOST=
    Environment=DATABASE_PORT=
    Environment=DATABASE_USER=
    Environment=DATABASE_PASSWORD=
    Environment=DATABASE_NAME=

    [Install]
    WantedBy=multi-user.target
    ```

5. Run it: `task-management.exe` or `systemctl start task-management`

6. Go to your browser, open `http://server:listen_port/` and enjoy it.

7. For production use, you will need to place the service behind a caching web server, such as *nginx*, which will allow you to use the *https* protocol instead of *http*. Below is an example of nginx configuration file `/etc/nginx/conf.d/domain.com.conf`:

    ```
    server {
        listen 80;
        listen 443 ssl;
        server_name todo.domain.com www.todo.domain.com;

        ssl on;
        ssl_certificate         /etc/nginx/ssl/domain.com/domain.crt;
        ssl_certificate_key     /etc/nginx/ssl/domain.com/private.key;
        ssl_trusted_certificate /etc/nginx/ssl/domain.com/intermediate.crt;

        location / {
            proxy_pass http://127.0.0.1:8001;
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
        }
    }
    ```

## How to create a database?

```
CREATE TABLE tasks (
	id UUID NOT NULL,
	email TEXT NOT NULL,
	content TEXT NOT NULL,
	private BOOLEAN NOT NULL DEFAULT false,
	status TEXT NOT NULL DEFAULT 'created',
	revision INTEGER NOT NULL DEFAULT 0,
	timestamp TIMESTAMP NOT NULL DEFAULT now(),
	external_id TEXT NOT NULL DEFAULT '',
	assigned TEXT NOT NULL DEFAULT '',
	term DATE NULL,
	icon TEXT NOT NULL DEFAULT '',
	project TEXT NOT NULL DEFAULT '',
	protocol TEXT NOT NULL DEFAULT '',
	parent_id UUID NULL,
    CONSTRAINT unique_task_id PRIMARY KEY (id)
)

CREATE TABLE todays (
	email TEXT NOT NULL,
	id UUID NOT NULL,
    index  DOUBLE PRECISION NOT NULL,
    todaystamp TIMESTAMP NOT NULL DEFAULT now(),
    delimiter boolean NOT NULL DEFAULT false,
    CONSTRAINT unique_today PRIMARY KEY (email, id)
)

```

## Who generates task IDs and their timestamps during the environment operations?

Unique task IDs are always generated on the **client side**. Usually this is done using the **UUID v4**. If the client tries to send tasks to the server without generated IDs, they will not be accepted by the server. Task ID is not a unique key of the table - only along with the user name. Therefore, different users may have tasks with the same ID.

Timestamps are always generated on the **server side**: by the date and time, when the task was updated in the database. All new tasks, that the client sends to the server, should not have a timestamp. Edited tasks, during transmission to the server, have information about the past value of the timestamp - to correctly solve collisions of editing same task by several clients. All tasks, that were updated on server during synchronization, are returned to the client application with new timestamps attached.